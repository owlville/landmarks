//
//  ContentView.swift
//  Landmarks
//
//  Created by JONATHAN HOLLAND on 8/7/19.
//  Copyright © 2019 Jonny. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            MapView().edgesIgnoringSafeArea(.top).frame(height: 300)
            CircleImage().offset(y: -130).padding(.bottom, -130)
            VStack(alignment: .leading) {
                Text("Turtle Rock")
                    .font(.title)
                HStack {
                    Text("Joshua Tree National Park").font(.subheadline)
                    Spacer()
                    Text("California").font(.subheadline)
                }//endOf: HStack {...}
            }//endOF: VStack {...}
        .padding()
            Spacer()
        }//endOf: VStack{...}
    }//endOf: var body {...}
}//endOf: ContentView {...}

#if DEBUG
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
#endif
